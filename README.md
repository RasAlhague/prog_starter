# prog_starter:

Prog_starter stands for program starter and is a tool for starting programs from the commandline. It is entirely written in Rust and doesn't uses unsafe code. Its made to start any program on the pc on any path. It uses crossplatform crates to enable using it under windows and *nix systemen.

## Idea:

 - Cross platform fast program starter.
 - starts programs accros all paths.
 - memory safe through rust

## Current features:

 - Start programs bei exact name.
   - starts only the first program found without a chance of choosing another.
 - Caches searches.
   - if once searched the search will be cached in a simple file, allowing to manually changing the files contents.
 

## Future features (Will be in the issues too):
 - an search index which will be keep track of a executable files at Pc startup.
 - an functionality which lets you choose if more than one file exists (first needs the index to be build.)

## Crossplatform: 

At the moment the only tested and compiled environment is Win10 Pro. In future tests are planned for Ubuntu and some other OS.

## Know Bugs/Problems(Will be in the issues too):

 - if a path of a once found file is changed, an error may occur, because its existens is not been checked again.
 - at the moment searching takes a lot of time because a lot of unneccesary files are looked through which slows the performance of the app.