use std::fs;
use std::path::Path;

pub struct CacheData {
    pub search_path: String,
    pub search_value: String,
    pub result: String,
}

impl CacheData {
    pub fn new(search_path: &str, search_value: &str, result: &str) -> CacheData {
        CacheData {
            search_path: String::from(search_path),
            search_value: String::from(search_value),
            result: String::from(result),
        }
    }
}

pub struct Cache {
	data: Vec<CacheData>,
}

impl Cache {
	pub fn read_cache(path: &str) -> Result<Cache, &'static str> {
		if !Path::new(path).exists() {
			match fs::write(path, "") {
				Ok(d) => d,
				Err(err) => {
					println!("Error while creating file: {}", err);
					return Err("Couldn't create file!");
				}
			};
		}
		let data = match fs::read_to_string(path) {
			Ok(d) => d,
			Err(err) => {
				println!("Error while creating file: {}", err);
				return Err("Couldn't create file!");
			}
		};

		let data = data.trim_end();

		let mut cache: Vec<CacheData> = Vec::new();

		if data.len() > 0 {
			let lines: Vec<&str> = data.split('\n').collect();

			for line in lines {
				let line_data: Vec<&str> = line.split(';').collect();

				cache.push(CacheData::new(line_data[0], line_data[1], line_data[2]));
			}
		}
		
		Ok(Cache {
			data: cache, 
		})
	}
	
	pub fn write(&self, path: &str) -> Result<(), &'static str> {
		let mut write_str: String = String::from("");

		for data in self.data.iter() {
			let line = format!(
				"{};{};{}\n",
				data.search_path, data.search_value, data.result
			);
			write_str.push_str(&line);
		}

		match fs::write(path, write_str) {
			Ok(_) => Ok(()),
			Err(err) => {
				println!("Error while creating file: {}", err);
				return Err("Couldn't create file!");
			}
		}
	}	
	
	pub fn contains(&self, search_path: &str, search_value: &str) -> bool {
		return self.data
			.iter()
			.find(move |x| x.search_path == search_path && x.search_value == search_value)
			.is_some();
	}
	
	pub fn try_get(&self, search_path: &str, search_value: &str) -> Option<&CacheData> {
		return self.data
            .iter()
            .find(move |x| x.search_path == search_path && x.search_value == search_value)
	}
	
	pub fn add_entry(&mut self, cache_entry: CacheData) {
		self.data.push(cache_entry);
	}
}