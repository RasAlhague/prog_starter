mod cache;

use std::process::Command;
use structopt::StructOpt;
use walkdir::WalkDir;
use std::time::Instant;
use crate::cache::Cache;
use crate::cache::CacheData;

#[derive(Debug, StructOpt)]
#[structopt(name = "prog_starter_rs")]
pub struct Config {
    #[structopt(short = "s", long = "search_value")]
    search_value: String,
    #[structopt(short = "p", long = "path")]
    path: Option<String>,
    #[structopt(short = "a", long = "args")]
    arguments: Option<String>,
}

impl Config {
    pub fn new() -> Config {
        Config::from_args()
    }
}

static CACHE_PATH: &str = "./cache.txt";

pub fn run(config: Config) {
    let path = match config.path {
        Some(p) => p,
        None => String::from("C:\\"),
    };
    let args = match &config.arguments {
        Some(a) => a.replace(":=", "-"),
        None => String::from(""),
    };
    let mut cache = match Cache::read_cache(CACHE_PATH) {
        Ok(c) => c,
        Err(err) => {
            println!("Err while reading cache: {}", err);
            return;
        }
    };

    if cache.contains(&path, &config.search_value)
    {
        let data = cache
			.try_get(&path, &config.search_value)
            .unwrap();

        match Command::new(&data.result).spawn() {
            Ok(_) => {
                println!("Startet program: {}!", data.result);
            }
            Err(err) => {
                println!("Couldn't start program, cause: {}", err);
            }
        }
    } else {
        let now = Instant::now();		
		let sv = config.search_value.clone();
        
        for entry in WalkDir::new(path.clone())
            .into_iter()
            .filter_map(Result::ok)
			.filter(move |x| x.file_name().to_str().unwrap() == sv )
        {
			match Command::new(entry.path()).spawn() {
				Ok(_) => {
					println!("Startet program after {} sec: {}!", now.elapsed().as_secs(), entry.path().display());

					cache.add_entry(CacheData::new(
						&path,
						&config.search_value,
						&entry.path().to_str().unwrap(),
					));

					match cache.write(CACHE_PATH) {
						Ok(_) => return,
						Err(err) => {
							println!("Couldn't write cache: {}", err);
							return;
						}
					};
				}
				Err(err) => {
					println!("Couldn't start program, cause: {}", err);
				}
			}
        }
    }
}
